package com.weatherapi.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
@JsonIgnoreProperties(ignoreUnknown = true) // bilinmeyenleri ignore
public class LocalTimeResponse {

    private LocalTimeResponseDto location;

    public LocalTimeResponseDto getLocation() {
        return location;
    }

    public void setLocation(LocalTimeResponseDto location) {
        this.location = location;
    }

    public LocalTimeResponse() {
    }
}

