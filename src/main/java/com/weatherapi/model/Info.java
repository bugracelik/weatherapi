package com.weatherapi.model;

public @interface Info {
    String info();
}
