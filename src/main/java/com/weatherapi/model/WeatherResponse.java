package com.weatherapi.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true) // bilinmeyenleri ignore
public class WeatherResponse {
    private CurrentDto current;

    public WeatherResponse() {
    }

    public CurrentDto getCurrent() {
        return current;
    }

    public void setCurrent(CurrentDto current) {
        this.current = current;
    }

}

