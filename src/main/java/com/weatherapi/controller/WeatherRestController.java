package com.weatherapi.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.weatherapi.model.Info;
import com.weatherapi.model.LocalTimeResponse;
import com.weatherapi.model.WeatherResponse;
import com.weatherapi.url.WeatherApiUrl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;


//todo : https://github.com/FasterXML/jackson-databind/issues/1886
//istek atmadan önce apı keyini kontrol et (https://www.weatherapi.com).

@Slf4j
@RestController
public class WeatherRestController {

    @Info(info = "burada weather api kullanıldı") // bilgi amaçlı işaret bırakmaca
    @GetMapping("{cityName}/withoutSpring")// frameworke işaret bırakırsın. algılasın, ona göre tomcati düzenlesin
    public ResponseEntity weatherInformationResponseEntity(@PathVariable String cityName) throws JsonProcessingException {
        RestTemplate restTemplate = new RestTemplate();
        String url = WeatherApiUrl.getWeatherApiUrlForJson(cityName);

        ResponseEntity<String> httpResponse = null;
        try {
            httpResponse = restTemplate.getForEntity(url, String.class);// http get request, http response 400 ise exception
        } catch (Exception e) {
            e.printStackTrace();
        }

        // HttpHeaders headers = httpResponse.getHeaders();
        // assert httpResponse != null;
        String responseBody = httpResponse.getBody(); // xml, json, html, raw baba, ftp, mail
        //Logger log = new Logger();
        log.info(responseBody);

        ObjectMapper objectMapper = new ObjectMapper();
        //string to model(deserialization)
        WeatherResponse weatherResponse = objectMapper.readValue(responseBody, WeatherResponse.class);
        String weatherResponseJson = objectMapper.writeValueAsString(weatherResponse);

        ResponseEntity responseEntity = new ResponseEntity(weatherResponseJson, HttpStatus.OK);
        return responseEntity;
    }

    @Info(info = "burada weather api kullanıldı") // bilgi amaçlı işaret bırakmaca
    @GetMapping("{city}") // frameworke işaret bırakırsın. algılasın, ona göre tomcati düzenlesin
    public WeatherResponse weatherInformation(@PathVariable String city) throws JsonProcessingException {

        String url = WeatherApiUrl.getWeatherApiUrlForJson(city);
        //string to model (deserialization)
        WeatherResponse weatherResponse = new RestTemplate().getForObject(url, WeatherResponse.class);

        //model to string (serialization)
        String weatherResponseJsonString = new ObjectMapper().writeValueAsString(weatherResponse);

        log.info(weatherResponseJsonString);

        Double temp_c = null;
        if (weatherResponse != null) {
            temp_c = weatherResponse.getCurrent().getTemp_c();
        }

        log.info("City: {} - Temp: {}", city, temp_c);

        return weatherResponse;
    }

    @Info(info = "burada weather api kullanıldı") // bilgi amaçlı işaret bırakmaca
    @GetMapping("{city}/withoutmapper") // frameworke işaret bırakırsın. algılasın, ona göre tomcati düzenlesin
    public String weatherInformation_without_mapper(@PathVariable String city) throws JsonProcessingException {
        String url = WeatherApiUrl.getWeatherApiUrlForJson(city);
        return new RestTemplate().getForEntity(url, String.class).getBody(); // temel
    }


    // http call atıyoruz.
    @Info(info = "burada weather api kullanıldı") // bilgi amaçlı işaret bırakmaca
    @GetMapping("{city}/xml") // frameworke işaret bırakırsın. algılasın, ona göre tomcati düzenlesin
    public String weatherInformation_return_xml(@PathVariable String city) throws JsonProcessingException {
        String url = WeatherApiUrl.getWeatherApiUrlForXml(city);
        return new RestTemplate().getForEntity(url, String.class).getBody(); // temel
    }

    @Info(info = "burada weather api yardımıyla localtime ögrenebiliyoruz")
    @GetMapping("localtime/{cityName}")
    public LocalTimeResponse getLocalTime(@PathVariable String cityName) throws JsonProcessingException {
        String url = WeatherApiUrl.getWeatherApiUrlForJson(cityName);
        return new RestTemplate().getForObject(url, LocalTimeResponse.class);
    }

    @Info(info = "burada weather api yardımıyla localtime ögrenebiliyoruz")
    @GetMapping("localtime/{cityName}/withEntity")
    public LocalTimeResponse getLocalTimeWithEntity(@PathVariable String cityName) throws JsonProcessingException {
        String url = WeatherApiUrl.getWeatherApiUrlForJson(cityName);
        String responseBody = new RestTemplate().getForEntity(url, String.class).getBody();
        //deserialization (string to model)
        return new ObjectMapper().readValue(responseBody, LocalTimeResponse.class);
    }

}



