package com.weatherapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WeatherApiSpring {

    public static void main(String[] args) {
        SpringApplication.run(WeatherApiSpring.class, args);
    }

}
