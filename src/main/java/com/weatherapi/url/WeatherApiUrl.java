package com.weatherapi.url;

public class WeatherApiUrl {
    // http://api.weatherapi.com/v1/current.json?key=3a285091fce743c0a05100233200110&q=İstanbul http call yapılacak url
    public static String getWeatherApiUrlForJson(String cityName) {
        return String.format("http://api.weatherapi.com/v1/current.json?key=3a285091fce743c0a05100233200110&q=%s", cityName);
    }

    public static String getWeatherApiUrlForXml(String cityName) {
        return String.format("http://api.weatherapi.com/v1/current.xml?key=3a285091fce743c0a05100233200110&q=%s", cityName);
    }
}
